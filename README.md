# [Neovim](https://neovim.io/)
This is my neovim config.

I'm using neovim pre-built [install](https://github.com/neovim/neovim/blob/master/INSTALL.md#:~:text=Linux-,Pre%2Dbuilt%20archives,-The%20Releases%20page).

Standard path config is in `~/.config/nvim`

base to config [apply](https://github.com/cpow/neovim-for-newbs/tree/main)

## Plugins manager

[Lazy](https://github.com/folke/lazy.nvim.git)

### Plugins

- ["lazy.nvim"](https://github.com/folke/lazy.nvim)
- ["catppuccin"](https://github.com/catppuccin/nvim)
- ["cmp-nvim-lsp"](https://github.com/hrsh7th/cmp-nvim-lsp)
- ["LuaSnip"](https://github.com/L3MON4D3/LuaSnip)
- ["cmp_luasnip"](https://github.com/saadparwaiz1/cmp_luasnip)
- ["friendly-snippets"](https://github.com/rafamadriz/friendly-snippets)
- ["nvim-cmp"](https://github.com/hrsh7th/nvim-cmp)
- ["vim-fugitive"](https://github.com/tpope/vim-fugitive)
- ["gitsigns.nvim"](https://github.com/lewis6991/gitsigns.nvim)
- ["indent-blankline.nvim"](https://github.com/lukas-reineke/indent-blankline.nvim)
- ["mason.nvim"](https://github.com/williamboman/mason.nvim)
- ["mason-lspconfig.nvim"](https://github.com/williamboman/mason-lspconfig.nvim)
- ["nvim-lspconfig"](https://github.com/neovim/nvim-lspconfig)
- ["lualine.nvim"](https://github.com/nvim-lualine/lualine.nvim)
- ["neo-tree.nvim"](https://github.com/nvim-neo-tree/neo-tree.nvim)
- ["plenary.nvim"](https://github.com/nvim-lua/plenary.nvim)
- ["nvim-web-devicons"](https://github.com/nvim-tree/nvim-web-devicons)
- ["nui.nvim"](https://github.com/MunifTanjim/nui.nvim)
- ["none-ls.nvim"](https://github.com/nvimtools/none-ls.nvim)
- ["vim-tmux-navigator"](https://github.com/christoomey/vim-tmux-navigator)
- ["oil.nvim"](https://github.com/stevearc/oil.nvim)
- ["conform.nvim"](https://github.com/stevearc/conform.nvim)
- ["swagger-preview.nvim"](https://github.com/vinnymeller/swagger-preview.nvim)
- ["telescope-ui-select.nvim"](https://github.com/nvim-telescope/telescope-ui-select.nvim)
- ["telescope.nvim"](https://github.com/nvim-telescope/telescope.nvim)
- ["nvim-treesitter"](https://github.com/nvim-treesitter/nvim-treesitter)
- ["vim-test"](https://github.com/vim-test/vim-test)
- ["vimux"](https://github.com/preservim/vimux)
